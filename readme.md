# Simple Crypto Server


## Notes

```sh
- please install metamask chrome plugin
- please use git bash or powershell if npm install is not a success on installing on cmd (node-gyp(web3) issue)
- please create fill up this 4 variables on .env file before running
  - INFURA_ACCESS_TOKEN
  -	WALLET_ADDRESS
  - WALLET_PRIVATE_KEY
  - DESTINATION_WALLET_ADDRESS
```


## How to run

```sh
git clone https://inovalcalabia@bitbucket.org/inovalcalabia/eth-web-server-demo.git
npm install
node transacation.js
```


## Author

👤 **inovalcalabia**

* Twitter: [@inovalcalabia](https://twitter.com/inovalcalabia)
* Github: [@inovalcalabia](https://github.com/inovalcalabia)
