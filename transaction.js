/**
 * Require the credentials that you entered in the .env file
 */
require('dotenv').config();

const MAINET_RPC_URL = 'https://mainnet.infura.io/'
const ROPSTEN_RPC_URL = 'https://ropsten.infura.io/'
const KOVAN_RPC_URL = 'https://kovan.infura.io/'
const RINKEBY_RPC_URL = 'https://rinkeby.infura.io/'
const LOCAL_RPC_URL = 'http://localhost:8545';


const testmode = true
const WebSocket = require('ws');
const Web3 = require('web3')
const axios = require('axios')
const EthereumTx = require('ethereumjs-tx')
const log = require('ololog').configure({ time: true })
const ansi = require('ansicolor').nice
const test_port = 8082;
const main_port = 8082;
const wss = new WebSocket.Server({ port: testmode ? test_port : main_port });


const S_GAME_START = 50;
const S_GAME_TIME = 100;
const S_GAME_END = 200;
const S_TRANSACTION_HASH = 250;

const C_PLAYER_INFO = 300;
const C_BET = 400;


const GAME_HEART_BEAT = 500;



function gamestart_package({ _server_address }) {
  return {id: S_GAME_START, server_address: _server_address};
}
function gametime_package({ _time, _isLock}) {
  return {id: S_GAME_TIME, time: _time, isLock: _isLock};
}
function gameend_package({ _winning_number, _won_price, _is_win }) {
  return {id: S_GAME_END, winning_number: _winning_number, won_price: _won_price, is_win: _is_win};
}
function gameheart_package() {
  return {id: GAME_HEART_BEAT, hb: '--heart-beat--'};
}

var maxTime = 40;
var lockGameTime = 20;
var inGameTime = maxTime;
var isLock = false;

wss.on('connection', function connection(ws) {
  console.log('someone connect to server, total playing: ', wss.clients.size);
  ws.on('message', function incoming(cMsg) {
    var cMsg = JSON.parse(cMsg);
    if (cMsg.id === C_PLAYER_INFO) {
      console.log('C_PLAYER_INFO: ', cMsg);
      ws.address = cMsg.address;
      ws.etc_transfer = new eth_transaction(process.env.WALLET_PRIVATE_KEY, process.env.WALLET_ADDRESS, cMsg.network_id, ws)
      ws.bets = [0, 0, 0, 0, 0, 0];

    } else if (cMsg.id === C_BET) {
      console.log('C_BET: ', cMsg);
      ws.bets = cMsg.bets; 
    }
  });
  ws.send(JSON.stringify(gamestart_package({_server_address: process.env.WALLET_ADDRESS})));
});


setInterval(() => {
  for(const client of wss.clients)
  {
    client.send(JSON.stringify(gametime_package({ _time: inGameTime, _isLock: isLock})));
  }

  inGameTime -= 1;
  if (inGameTime < 0 && !isLock) {
    inGameTime = lockGameTime;
    isLock = true;
    // var winningNumber = getWinNumber();
    for(const client of wss.clients) {
      var winningNumber = validateBet(client.bets);
      if(winningNumber.value === 0) {
        client.send(JSON.stringify(gameend_package({_winning_number: winningNumber.index, _won_price: 0, _is_win: false})));
      } else {
        client.send(JSON.stringify(gameend_package({_winning_number: winningNumber.index, _won_price: winningNumber.value * 2, _is_win: true})));
        if (client.etc_transfer) {
          client.etc_transfer.transac(client.address, winningNumber.value * 2);
        }
      }
    }
  }
  if (inGameTime < 0 && isLock) {
    inGameTime =  maxTime;
    isLock = false;
    reset();
  }
}, 1000);

var playerProps = {
  address:'',
  betlist: [{
    index: 0,
    price: 0
  }],
}
var playerList = [];


function reset() {
  for(const client of wss.clients) {
    client.bets = [0, 0, 0, 0, 0, 0];
  }
   
}
function findLowest( array ){
    return Math.min.apply( Math, array );
}
function isAllSame(array) {
  return array.every((val, i, arr) => val === arr[0]);
}

function getAllIndexes(arr, val) {
    var indexes = [], i = -1;
    while ((i = arr.indexOf(val, i+1)) != -1){
        indexes.push(i);
    }
    return indexes;
}

function validateBet(arr) {
  if (arr === undefined) return 0;
  if (isAllSame(arr)) {
    if(arr[0] === 0) {
      var index = Math.floor(Math.random() * arr.length);
      return {value: arr[index], index: index}
    } else {
      var index = Math.floor(Math.random() * arr.length);
      return {value: arr[index], index: index}
    }
  } else {
    var lowest = findLowest(arr);
    if (lowest === 0) {
      var listOfList = getAllIndexes(arr, lowest)
      var index = Math.floor(Math.random() * listOfList.length);
      return {value: arr[listOfList[index]], index: listOfList[index]};
    } else {
      var listOfList = getAllIndexes(arr, lowest)
      var index = Math.floor(Math.random() * listOfList.length);
      return {value: arr[listOfList[index]], index: listOfList[index]};
    }
  }
  return 0;
}
// if win = 0 do nothing
// if win > 0 multply by 2 send to client
function getWinNumber() {
  var listOfNUmbers = [1, 2, 3, 4, 5, 6];
  var index = Math.floor(Math.random() * listOfNUmbers.length);
  return listOfNUmbers[index];
}

const mainnet = `https://mainnet.infura.io/${process.env.INFURA_ACCESS_TOKEN}`
const ropstennet = `https://ropsten.infura.io/${process.env.INFURA_ACCESS_TOKEN}`
const kovannet = `https://kovan.infura.io/${process.env.INFURA_ACCESS_TOKEN}`
const rinkebynet = `https://rinkeby.infura.io/${process.env.INFURA_ACCESS_TOKEN}`
const testnet = 'http://localhost:8545';

const networkToUse = testmode ? testnet : mainnet
const web3 = new Web3( new Web3.providers.HttpProvider(networkToUse) )


function setWeb3(network_id) {
  let network = `http://localhost:8545`;
  if (network_id === "1") {
    network = `https://mainnet.infura.io/${process.env.INFURA_ACCESS_TOKEN}`;
  } else if (network_id === "2") {
    network = `https://ropsten.infura.io/${process.env.INFURA_ACCESS_TOKEN}`;
  } else if (network_id === "3") {
    network = `https://kovan.infura.io/${process.env.INFURA_ACCESS_TOKEN}`
  } else if (network_id === "4") {
     network = `https://rinkeby.infura.io/${process.env.INFURA_ACCESS_TOKEN}`
  }  
  return new Web3( new Web3.providers.HttpProvider(network) )
}

web3.eth.defaultAccount = process.env.WALLET_ADDRESS


const amountToSend = testmode ? 1 : 0.00010000 // 0.00010000 

const getCurrentGasPrices = async () => {
  let response = await axios.get('https://ethgasstation.info/json/ethgasAPI.json')
  let prices = {
    low: response.data.safeLow / 10,
    medium: response.data.average / 10,
    high: response.data.fast / 10
  }

  console.log("\r\n")
  log (`Current ETH Gas Prices (in GWEI):`.cyan)
  console.log("\r\n")
  log(`Low: ${prices.low} (transaction completes in < 30 minutes)`.green)
  log(`Standard: ${prices.medium} (transaction completes in < 5 minutes)`.yellow)
  log(`Fast: ${prices.high} (transaction completes in < 2 minutes)`.red)
  console.log("\r\n")

  return prices
}


var eth_transaction = function(_private_key, _my_address, _network_id, _ws) {
   this.private_key = _private_key;
   this.my_address = _my_address;
   this.network_id = _network_id;
   this.ws = _ws
   this.findWeb3 = (network_id) => {
      let network = `http://localhost:8545`;
      if (network_id === "1") {
        network = `https://mainnet.infura.io/${process.env.INFURA_ACCESS_TOKEN}`;
      } else if (network_id === "2") {
        network = `https://ropsten.infura.io/${process.env.INFURA_ACCESS_TOKEN}`;
      } else if (network_id === "3") {
        network = `https://kovan.infura.io/${process.env.INFURA_ACCESS_TOKEN}`
      } else if (network_id === "4") {
         network = `https://rinkeby.infura.io/${process.env.INFURA_ACCESS_TOKEN}`
      }
      log(network)
      return new Web3( new Web3.providers.HttpProvider(network) )
  
   }
   this.web3 = this.findWeb3(this.network_id);
   this.getCurrentGasPrices = async () => {
    let response = await axios.get('https://ethgasstation.info/json/ethgasAPI.json')
    let prices = {
      low: response.data.safeLow / 10,
      medium: response.data.average / 10,
      high: response.data.fast / 10
    }

    console.log("\r\n")
    log (`Current ETH Gas Prices (in GWEI):`.cyan)
    console.log("\r\n")
    log(`Low: ${prices.low} (transaction completes in < 30 minutes)`.green)
    log(`Standard: ${prices.medium} (transaction completes in < 5 minutes)`.yellow)
    log(`Fast: ${prices.high} (transaction completes in < 2 minutes)`.red)
    console.log("\r\n")

    return prices
  }
  this.transac = async (_destination_wallet, _amount) => {

    let destination_wallet = _destination_wallet;
    let amount = _amount;
    /**
     * Fetch the balance of the destination address
     */
    let destinationBalanceWei = this.web3.eth.getBalance(destination_wallet).toNumber()
    let destinationBalance = this.web3.fromWei(destinationBalanceWei, 'ether')

    log(`Destination wallet balance is currently ${destinationBalance} ETH`.green)


    /**
     * Fetch your personal wallet's balance
     */
    let myBalanceWei = this.web3.eth.getBalance(this.my_address).toNumber()
    let myBalance = this.web3.fromWei(myBalanceWei, 'ether')

    log(`Your wallet balance is currently ${myBalance} ETH`.green)


    /**
     * With every new transaction you send using a specific wallet address,
     * you need to increase a nonce which is tied to the sender wallet.
     */
    let nonce = this.web3.eth.getTransactionCount(this.my_address)
  /*  this.web3.eth.getTransactionCount(accounts[0], (x, e)=> {
      nonce = e;
    });*/
    log(`The outgoing transaction count for your wallet address is: ${nonce}`.magenta)


    /**
     * Fetch the current transaction gas prices from https://ethgasstation.info/
     */
    let gasPrices = await getCurrentGasPrices()


    /**
     * Build a new transaction object and sign it locally.
     */
    let details = {
      "to": destination_wallet,
      "value": this.web3.toHex( this.web3.toWei(amount, 'ether') ),
      "gas": 21000,
      "gasPrice": gasPrices.low * 1000000000, // converts the gwei price to wei
      "nonce": nonce,
      "chainId": parseFloat(this.network_id)// testmode ? 4 : 1 // EIP 155 chainId - mainnet: 1, rinkeby: 4
    }

    const transaction = new EthereumTx(details)


    /**
     * This is where the transaction is authorized on your behalf.
     * The private key is what unlocks your wallet.
     */
    transaction.sign( Buffer.from(this.private_key, 'hex') )


    /**
     * Now, we'll compress the transaction info down into a transportable object.
     */
    const serializedTransaction = transaction.serialize()

    /**
     * Note that the this.web3 library is able to automatically determine the "from" address based on your private key.
     */

    // const addr = transaction.from.toString('hex')
    // log(`Based on your private key, your wallet address is ${addr}`)

    /**
     * We're ready! Submit the raw transaction details to the provider configured above.
     */
    const transactionId = this.web3.eth.sendRawTransaction('0x' + serializedTransaction.toString('hex') )

    /**
     * We now know the transaction ID, so let's build the public Etherscan url where
     * the transaction details can be viewed.
     */
    // const url = `https://rinkeby.etherscan.io/tx/${transactionId}`
    const url = `https://etherscan.io/tx/${transactionId}`
    log(url.cyan)
    if (this.network_id === "1") {
      this.ws.send(JSON.stringify({id: S_TRANSACTION_HASH, url: 'https://etherscan.io/tx/' + transactionId}));
    } else if (this.network_id === "2") {
      this.ws.send(JSON.stringify({id: S_TRANSACTION_HASH, url: 'https://ropsten.etherscan.io/tx/' + transactionId}));
    } else if (this.network_id === "3") {
      this.ws.send(JSON.stringify({id: S_TRANSACTION_HASH, url: 'https://kovan.etherscan.io/tx/' + transactionId}));
    } else if (this.network_id === "4") {
      this.ws.send(JSON.stringify({id: S_TRANSACTION_HASH, url: 'https://rinkeby.etherscan.io/tx/' + transactionId}));
    } 
    log(`Note: please allow for 30 seconds before transaction appears on Etherscan`.magenta)
  } 
}










