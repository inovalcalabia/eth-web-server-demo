// id(500) = heart beat
// id(100) = time
// id(200) = winner

const WebSocket = require('ws');
const Web3 = require('web3')
const axios = require('axios')
const EthereumTx = require('ethereumjs-tx').Transaction
const log = require('ololog').configure({ time: true })
const ansi = require('ansicolor').nice

const MAINET_RPC_URL = 'https://mainnet.infura.io/metamask'
const ROPSTEN_RPC_URL = 'https://ropsten.infura.io/metamask'
const KOVAN_RPC_URL = 'https://kovan.infura.io/metamask'
const RINKEBY_RPC_URL = 'https://rinkeby.infura.io/metamask'
const LOCAL_RPC_URL = 'http://localhost:8545';



const INFURA_ACCESS_TOKEN='7b14472a33a34e60a4c637982fa716c0'

const WALLET_ADDRESS='0x2b06F49Ab17Ba82A56b902e8E47f7227401623C2'

const WALLET_PRIVATE_KEY= 'D9B1D4171D43220ED314FC8688B13B055408CA74D691F67550ECDA48B696D82E'

const DESTINATION_WALLET_ADDRESS='0xf071cAd52e3b0eDbBD5D94EebfB318535667A249'







const testnet = 'https://rinkeby.infura.io/' + INFURA_ACCESS_TOKEN + '';

// const web3 = new Web3( new Web3.providers.HttpProvider(MAINET_RPC_URL) )
const web3 = new Web3( new Web3.providers.HttpProvider(testnet) )
const amountToSend = 0.0001;
web3.eth.defaultAccount = WALLET_ADDRESS;

const main = async () => {
  // our code goes here
  // let myBalanceWei = web3.eth.getBalance(web3.eth.defaultAccount)
  web3.eth.getBalance(WALLET_ADDRESS, (err, balance) => {
	myBalanceWei = balance
	let myBalance = web3.utils.fromWei(myBalanceWei, 'ether')
	  log(`Your wallet balance is currently ${myBalance} ETH`.green)
  });
  let nonce = web3.eth.getTransactionCount(WALLET_ADDRESS)
  const getCurrentGasPrices = async () => {
  let response = await axios.get('https://ethgasstation.info/json/ethgasAPI.json')
  let prices = {
	    low: response.data.safeLow / 10,
	    medium: response.data.average / 10,
	    high: response.data.fast / 10
	  }
	 
	  console.log("\r\n")
	  log (`Current ETH Gas Prices (in GWEI):`.cyan)
	  console.log("\r\n")
	  log(`Low: ${prices.low} (transaction completes in < 30 minutes)`.green)
	  log(`Standard: ${prices.medium} (transaction completes in < 5 minutes)`.yellow)
	  log(`Fast: ${prices.high} (transaction completes in < 2 minutes)`.red)
	  console.log("\r\n")
	 
	  return prices
	}
  let gasPrices = await getCurrentGasPrices()
 /* let details = {
  	"from": WALLET_ADDRESS,
    "to": DESTINATION_WALLET_ADDRESS,
    "value": web3.utils.toHex(web3.utils.toWei(String(amountToSend), 'ether')),
    "gas": web3.utils.toHex(21000),
    "gasPrice": web3.utils.toHex(gasPrices.low * 1000000000), // converts the gwei price to wei
    "nonce": web3.utils.toHex(nonce),
    "data": "",
     "chainId": 4 // EIP 155 chainId - mainnet: 1, rinkeby: 4
  }*/
  let details = {
    "to": DESTINATION_WALLET_ADDRESS,
    "value": web3.utils.toHex( web3.utils.toWei(String(amountToSend), 'ether') ),
    "gasLimit": '0x2710',// web3.utils.toHex(21000),
    "gasPrice": '0x09184e72a000',// web3.utils.toHex(gasPrices.low * 1000000000), // converts the gwei price to wei
    "nonce": web3.utils.toHex(nonce),
    "data": "",
    //"chainId": 4 // EIP 155 chainId - mainnet: 1, rinkeby: 4
  }


 const transaction = new EthereumTx(details)
 const privateKey = Buffer.from(
  WALLET_PRIVATE_KEY,
  'hex',
)
 // var privKey = new Buffer(WALLET_PRIVATE_KEY, 'hex');
   transaction.sign(privateKey)
  const serializedTransaction = transaction.serialize();
  log(`serializedTransaction ${serializedTransaction.toString('hex')}`);
  const transactionId = web3.eth.sendSignedTransaction('0x' + serializedTransaction.toString('hex'));
  

  const url = `https://rinkeby.etherscan.io/tx/${transactionId}`
  log(url.cyan)

  log(`Note: please allow for 30 seconds before transaction appears on Etherscan`.magenta)

process.exit()

}
 
main()




















// console.log(web3); 
const wss = new WebSocket.Server({ port: 8082 });
var maxTime = 20;
var inGameTime = maxTime;
var isLock = false;

wss.on('connection', function connection(ws) {
 console.log('someone connect to server');
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
    //ws.message = message;
  });
});


setInterval(() => {
	// console.log('client length', wss.clients.size);
	for(const client of wss.clients)
	{
	  client.send(JSON.stringify({id: 100 ,time: inGameTime, isLock: isLock}));
	}

	inGameTime -= 1;
	if (inGameTime <= 0 && !isLock) {
		inGameTime = 10;
		isLock = true;
		var winningNumber = getWinNumber();
		for(const client of wss.clients)
		{
		  client.send(JSON.stringify({id: 200 ,winningNumber: winningNumber}));
		}
		// console.log('winningNumber', winningNumber);
	}
	if (inGameTime <= 0 && isLock) {
		inGameTime =  maxTime;
		isLock = false;
	}
}, 1000);

var playerProps = {
	address:'',
	betlist: [{
		index: 0,
		price: 0
	}],
}
var playerList = [];

function getWinNumber() {
	var listOfNUmbers = [0, 1, 2, 3, 4, 5];
	var index = Math.floor(Math.random() * listOfNUmbers.length);
	return listOfNUmbers[index];
}
function reset() {

}
var balance = 0;
function getBalance() {
	web3.eth.getBalance(WALLET_ADDRESS, (err, balance) => {
		balance = web3.utils.fromWei(balance, "ether") + " ETH " + balance
		console.log(balance);
	});
}

 getBalance();

function sendMoney() {
	web3.eth.sendTransaction({
	    from: WALLET_ADDRESS,
	    to: "0xf071cAd52e3b0eDbBD5D94EebfB318535667A249", 
	    value: web3.toWei(0.0001, "ether"), 
	}, function(err, transactionHash) {
	    if (err) { 
	        console.log(err); 
	    } else {
	        console.log(transactionHash);
	        getBalance();
	    }
	});
}

// sendMoney();
